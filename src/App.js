import React, {useState,useEffect } from 'react';
import ReactPlayer from 'react-player'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons'
import { CarouselProvider, Slider, Slide } from 'pure-react-carousel';
import { Layout, Row, Col} from 'antd';
import ReviewSlide from './components/review/reviewSlide'
import ReviewItem from './components/review/reviewItem'
import Review from './example.json'


import './App.css'
import './icon.css'

let count =1;

function App() {

 /*  
  const insta_data =[
    {
      "id":0,
      "name":"Mijin Kim",
      "rating":4,
      "contents":"Soju Haus 최고입니다! "
  
  },
  {
      "id":1,
      "name":"Iraklisa Postolidis",
      "rating":4,
      "contents":"Best Gutenberg block collection ever made since yet. Powerful blocks. I hope to see more and more new free useful blocks. "
  
  },
  
  {
    "id":2,
    "name":"Iraklisa Postolidis2222",
    "rating":3,
    "contents":"Best Gutenberg"
  
  }
  
  ]
 */

  const insta_data = Review.reviews;

  const [review,setReview] = useState(insta_data[0]);
  
  useEffect(()=>{

    const interval2 = setInterval(()=>{

      handeleSlide()
  
    },8000)

    return ()=> {

      clearInterval(interval2)

    }

  },[])

  const handeleSlide=()=>{

    
    const list_num = count < insta_data.length ?  count : 0
    
    const item = insta_data[list_num]
   
    setReview(item)
  
    if(count < insta_data.length ){
    
     count = count+1
    
   
    
    } else{
    
      count =0
    
    } 
}
 

  return (

    <>
      <Layout>
     
        <Row className="content__row">
          <Col span={8}>
            <img className="menu" src="/1-3col.jpg" alt="menu" width="100%"></img>
          </Col>
          <Col span={8}>
            <div className="video">
              <ReactPlayer url="/video.mp4"  width="100%" height="100%" controls={true} playing={true} loop={true}/> 
            </div>
          </Col>
          <Col span={8}>
          
          <ReviewItem ritem={review}/>


        {/* 
          <ReviewSlide review={review} />  
      */}
          {/* <CarouselProvider naturalSlideWidth={100} naturalSlideHeight={125} totalSlides={2} isPlaying={true}>
                  <div className="review">
              <h3>SOJUHAUS</h3>
              <h4>WRITE US REVIEW ON GOOGLE!</h4>
        <Slider>
          <Slide index={0}>
            <div className="bf-testimonial-slick-single">
              <div className="bf-testimonial-icon">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStarHalfAlt} />
              </div>
              <div className="bf-testimonial-message">
                <p>
                  Soju Haus 최고입니다! 
                </p>
              </div>
              <div className="bf-testimonial-author-info">
                <div className="author-name"><h4>Mijin Kim</h4></div>
              </div>
            </div>
          </Slide>
          <Slide index={1}>
            <div className="bf-testimonial-slick-single">
              <div className="bf-testimonial-icon">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStarHalfAlt} />
              </div>
              <div className="bf-testimonial-message">
                <p>
                  Best Gutenberg block collection ever made since yet. 	Powerful blocks. I hope to see more and more new free useful blocks.
                </p>
              </div>
              <div className="bf-testimonial-author-info">
                <div className="author-name"><h4>Iraklisa Postolidis</h4></div>
              </div>
              
            </div>
          </Slide>

        </Slider>
        </div>
      </CarouselProvider> */}

            <div className="promotion">
              <img src="/promo.gif" alt="promotion" width="100%"></img>
            </div>
          </Col>
        </Row>
    </Layout>

    </>
   

  );
}



export default App;
