import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons'

const ReviewItem = ({ritem}) => {
    return (
        <div className="bf-testimonial-slick-single">
        <div className="bf-testimonial-icon">
        {(() => {
                 if( {ritem}.ritem.rating === 4){
                     return (
                         <>
                        <FontAwesomeIcon icon={faStar} />
                        <FontAwesomeIcon icon={faStar} />
                        <FontAwesomeIcon icon={faStar} />
                        <FontAwesomeIcon icon={faStar} />
                        <FontAwesomeIcon icon={faStarHalfAlt} />
                        </>
                     )
                 } else if ({ritem}.ritem.rating === 5){
                        return (
                        <>
                            <FontAwesomeIcon icon={faStar} />
                            <FontAwesomeIcon icon={faStar} />
                            <FontAwesomeIcon icon={faStar} />
                            <FontAwesomeIcon icon={faStar} />
                            <FontAwesomeIcon icon={faStar} />
                        </>)
                 }
        })()} 
        </div>
        <div className="bf-testimonial-message">
          <p>
            {ritem.contents}
          </p>
        </div>
        <div className="bf-testimonial-author-info">
        <div className="author-name"><h4>{ritem.name}</h4></div>
        </div>
      </div>
    )
} 

export default ReviewItem