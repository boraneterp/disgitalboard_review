import React from 'react';
import ReviewItem from './reviewItem'

import { CarouselProvider, Slider, Slide } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

const ReviewSlide = ({review}) => {
    return (

        <CarouselProvider naturalSlideWidth={100} naturalSlideHeight={125} totalSlides={2} isPlaying={true}>
            <div className="review">
                <h3>SOJUHAUS</h3>
                <h4>WRITE US REVIEW ON GOOGLE!</h4>
                <Slider>
                {
                    review.map((item,i)=>{
                        return(
                            <Slide index={i}>
                                <ReviewItem ritem={item} key={i}/>
                            </Slide>
                    )
                })
                }
                </Slider>
            </div>
        </CarouselProvider>   

    )
} 

export default ReviewSlide